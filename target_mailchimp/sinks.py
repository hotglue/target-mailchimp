"""Mailchimp target sink class, which handles writing streams."""


from email import header
from singer_sdk.sinks import BatchSink
import mailchimp_marketing as MailchimpMarketing
from mailchimp_marketing.api_client import ApiClientError
import requests


class MailchimpSink(BatchSink):
    """Mailchimp target sink class."""

    max_size = 10000  # Max records to write in one batch
    list_id = None
    all_members = []
    server = None
    def start_batch(self, context: dict) -> None:
        """Start a batch.
        
        Developers may optionally add additional markers to the `context` dict,
        which is unique to this batch.
        """
        # Sample:
        # ------
        # batch_key = context["batch_id"]
        # context["file_path"] = f"{batch_key}.csv"
        try:
            client = MailchimpMarketing.Client()
            server = self.get_server()
            client.set_config({
                "access_token": self.config.get('access_token'),
                "server": server
            })

            response = client.lists.get_all_lists()
            config_name = self.config.get('list_name')
            if 'lists' in response:
                for row in response['lists']:
                    if row['name'] == config_name:
                        self.list_id = row['id']

            print(response)
        except ApiClientError as error:
            print("Error: {}".format(error.text))
        starting_batch = 'bb'

    def get_server(self):
        if self.server is None:
            self.server = self.get_server_meta_data()
            
        return self.server    
            

    def get_server_meta_data(self):
        header = {
            "Authorization": f"OAuth {self.config.get('access_token')}"
        }
        metadata = requests.get("https://login.mailchimp.com/oauth2/metadata",headers=header).json()
        return metadata["dc"]

    def process_record(self, record: dict, context: dict) -> None:
        """Process the record.

        Developers may optionally read or write additional markers within the
        passed `context` dict from the current batch.
        """
        # Sample:
        # ------
        # with open(context["file_path"], "a") as csvfile:
        #     csvfile.write(record)
        first_name, *last_name  = record["FullName"].split()
        last_name=" ".join(last_name)
        self.all_members.append(
            {
                "email_address":record["Username"],
                "status":self.config.get('subscribe_status','subscribed'),
                "merge_fields":{
                    "FNAME":first_name,
                    "LNAME":last_name
                }

            }
        )

    def process_batch(self, context: dict) -> None:
        """Write out any prepped records and return once fully written."""
        if self.list_id is not None:
            try:
                client = MailchimpMarketing.Client()
                client.set_config({
                "access_token": self.config.get('access_token'),
                "server": self.get_server()
                })

                response = client.lists.batch_list_members(self.list_id, {"members": self.all_members})
                print(response)
            except ApiClientError as error:
                print("Error: {}".format(error.text))
