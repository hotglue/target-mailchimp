"""Mailchimp target class."""

from singer_sdk.target_base import Target
from singer_sdk import typing as th

from target_mailchimp.sinks import (
    MailchimpSink,
)


class TargetMailchimp(Target):
    """Sample target for Mailchimp."""

    name = "target-mailchimp"
    config_jsonschema = th.PropertiesList(
        th.Property(
            "access_token",
            th.StringType,
            description="access_token from oAuth",
            required=True
        ),
        th.Property(
            "list_name",
            th.StringType,
            required=True
        ),
    ).to_dict()
    default_sink_class = MailchimpSink

if __name__ == '__main__':
    TargetMailchimp.cli()    
